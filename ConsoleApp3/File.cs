﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class File
    {
        private int[] tableau;
        private int indexIn;
        private int indexOut;
        private int nbrElement;
        public File()
        {
            this.tableau = new int[10];
            this.indexIn = 9;
            this.indexOut = 9;
            this.nbrElement = 0;
        }
        public bool EstPleine()
        {
            if (this.nbrElement == 10)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool EstVide()
        {
            if (this.nbrElement == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void Enfiler(int valeur)
        {
            if(EstPleine()==false)
            {
                this.tableau[indexIn] = valeur;
                Console.WriteLine("Entre de {0}", valeur);
                nbrElement++;
                if(indexIn==0)
                {
                    indexIn = 9;
                }
                else
                {
                    indexIn -= 1;
                }
            }
            else
            {
                throw new Exception("File pleine"); 
            }
        }
        public void Defiler()
        {
            if(EstVide()==false)
            {
                Console.WriteLine("Sortie de {0}", tableau[indexOut]);
                nbrElement --;
                tableau[indexOut] = 0;
                if (indexOut == 0)
                {
                    indexOut = 9;
                }
                else
                {
                    indexOut -= 1;
                }
            }
            else
            {
                throw new Exception("File vide");
            }
        }
    }
}
